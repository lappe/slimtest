<!DOCTYPE html>
<html lang="en">
  <head>
	  <title>Atea Dashboard</title>
	  <? include('meta.php')?>
	  <!-- Bootstrap core CSS -->
	  <link href="<? echo $baseUrl; ?>/css/bootstrap.min.css" rel="stylesheet">
	  <link href="<? echo $baseUrl; ?>/css/custom.css" rel="stylesheet">
	  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	  <!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	  <![endif]-->
 </head>
  <body>
	<div class="navbar navbar-inverse navbar-fixed-top">
	  <div class="container">
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="<? echo $baseUrl; ?>/">
		      
		  	<img class="img-responsive2" src="<? echo $baseUrl; ?>/img/dashboardlogo.png"></a>
	    </div>
	    <div class="collapse navbar-collapse">
	      <ul class="nav navbar-nav">
	        <li><a href="<? echo $baseUrl; ?>/">Home</a></li>
	        <li><a href="<? echo $baseUrl; ?>/side2">Side 2</a></li>
	        <li><a href="<? echo $baseUrl; ?>/oversigt">Oversigt</a></li>
	        <li><a href="<? echo $baseUrl; ?>/findesikke">Findes ikke</a></li>
	        
	      </ul>
	      <p class="navbar-text navbar-right">Logget ind som <a href="#" class="navbar-link">Ukendt bruger</a></p>
	    </div><!--/.nav-collapse -->
	  </div>
	</div>
	
	<div class="container">  
		<?
			if(isset($page) && $page != ''){
				$page = $page .'.php';
			}
			else {
				$page = 'frontpage';
				$page = $page .'.php';
			}
			include($page);
		?> 
	</div><!-- /.container -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="<? echo $baseUrl; ?>/js/bootstrap.min.js"></script>
  </body>
</html>